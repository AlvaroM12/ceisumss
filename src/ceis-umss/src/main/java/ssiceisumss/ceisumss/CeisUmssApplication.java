package ssiceisumss.ceisumss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CeisUmssApplication {

    public static void main(String[] args) {
        SpringApplication.run(CeisUmssApplication.class, args);
    }
}
