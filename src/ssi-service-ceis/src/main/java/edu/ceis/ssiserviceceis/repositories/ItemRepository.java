package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
    