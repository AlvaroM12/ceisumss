package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Position;
import org.springframework.data.repository.CrudRepository;

public interface PositionRepository extends CrudRepository<Position, Long> {
}
    