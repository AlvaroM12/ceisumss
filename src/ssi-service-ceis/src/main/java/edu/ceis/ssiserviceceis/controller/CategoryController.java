package edu.ceis.ssiserviceceis.controller;

import edu.ceis.ssiserviceceis.repositories.CategoryRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CategoryController {
    private CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @RequestMapping("/categorys")
    public String getCategorys(Model model) {
        model.addAttribute("categorys", categoryRepository.findAll());
        return "categorys"; //es el nombre de una pagina html
    }
}
