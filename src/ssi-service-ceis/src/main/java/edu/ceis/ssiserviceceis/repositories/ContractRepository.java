package edu.ceis.ssiserviceceis.repositories;

import edu.ceis.ssiserviceceis.domain.Contract;
import org.springframework.data.repository.CrudRepository;

public interface ContractRepository extends CrudRepository<Contract, Long> {
}
    